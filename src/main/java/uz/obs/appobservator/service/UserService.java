package uz.obs.appobservator.service;


import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.obs.appobservator.repository.UserRepo;
import uz.obs.appobservator.dto.response.UserRes;
import uz.obs.appobservator.entity.User;
import uz.obs.appobservator.security.*;


import javax.management.relation.RoleInfo;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepo userRepo;
    private final PasswordEncoder passwordEncoder;

    public ApiResponse saveUser(User user, UserRes dto)
    {
        User user1 = new User();
        user1.setRole(RoleName.ROLE_ADMIN);
        user1.setPassword(passwordEncoder.encode(dto.getPassword()));
        user1.setName(dto.getName());
        user1.setLogin(dto.getUsername());
        if (dto.getId()!=null) user1.setId(dto.getId());
        User savedUser = userRepo.save(user1);
        return new ApiResponse("Muvaffaqiyatli " + (dto.getId()==null?"saqlandi":"qo'shildi"),true,savedUser);
    }
    public List<User> getAllUserRoleUser()
    {
        return  userRepo.findAll()
                .stream()
                .filter(user -> user.getRole() == RoleName.ROLE_USER)
                .collect(Collectors.toList());
    }
    public ApiResponse getUser()
    {
        return new ApiResponse("Barcha roli user bo'lgan foydalanuvchilar",true,getAllUserRoleUser());
    }

    public ApiResponse deleteUser(User user,Integer id){
        if(user.getRole().equals(RoleName.ROLE_SUPER_ADMIN)) {
            userRepo.deleteById(id);
            return new ApiResponse("foydalanuvchi o'chirildi", true);
        }
        return new ApiResponse("sizga ushbu yo'ga ruhsat yo'q",false);
    }

    public ApiResponse getUserAll() {
        return new ApiResponse("Barcha foydalanuvchilar",true,userRepo.findAll());
    }

    public ApiResponse changePassword(String login, String oldPassword, String newPassword) {
        User user = userRepo.findByLogin(login);
        if (user != null) {
            if (passwordEncoder.matches(oldPassword, user.getPassword())) {
                user.setPassword(passwordEncoder.encode(newPassword));
                userRepo.save(user);
                return new ApiResponse("parol o'zgartirildi",true);
            } else {
                return new ApiResponse("eski parol noto'g'ri",false);
            }
        } else {
            return new ApiResponse("bunday loginli faoydalanuvchi yo'q",false);
        }
    }

}
