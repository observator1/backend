package uz.obs.appobservator.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.obs.appobservator.dto.request.RegionDto;
import uz.obs.appobservator.dto.response.ApiResponse;
import uz.obs.appobservator.entity.Region;
import uz.obs.appobservator.repository.RegionRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RegionService {
    private final RegionRepository regionRepository;
    // REGION QO'SHISH UCHUN
    public ApiResponse addRegion(RegionDto regionDto) {
        Region region = new Region();
        region.setRegionName(regionDto.getRegionName());
        region.setRegionCode(regionDto.getRegionCode());
        Region saveRegion = regionRepository.save(region);
        return new ApiResponse("Region saqlandi", true, saveRegion);
    }
    // REGIONLAR LISTINI OLISH UCHUN
    public ApiResponse getRegion() {
        List<Region> regionList = regionRepository.findAll();
        return new ApiResponse("Regionlar listi", true, regionList);
    }

    //REGIONNI TAHRIRLASH
    public Optional<Region> updateRegion(Integer id, RegionDto updatedRegionDto) {
        Optional<Region> optionalRegion = regionRepository.findById(id);
        if (optionalRegion.isPresent()) {
            Region region = optionalRegion.get();
            region.setRegionName(updatedRegionDto.getRegionName());
            region.setRegionCode(updatedRegionDto.getRegionCode());
            regionRepository.save(region);
        }
        return optionalRegion;
    }

    // REGIONNI O'CHIRISH
    public boolean deleteRegion(Integer id) {
        if (regionRepository.existsById(id)) {
            regionRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
