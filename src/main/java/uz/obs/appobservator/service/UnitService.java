package uz.obs.appobservator.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.obs.appobservator.dto.request.UnitDto;
import uz.obs.appobservator.dto.response.ApiResponse;
import uz.obs.appobservator.entity.Unit;
import uz.obs.appobservator.repository.UnitRepository;

import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class UnitService {
    private final UnitRepository unitRepository;

    // UNIT QO'SHISH UCHUN
    public HttpEntity<?>addUnit(UnitDto unitsDto){
        Unit unit=new Unit();
        unit.setName(unitsDto.getName());
        unit.setCode(unitsDto.getCode());

        Unit saveUnits=unitRepository.save(unit);

        ApiResponse response=new ApiResponse();
        response.setMessage("unit saqlandi");
        response.setObj(saveUnits);
        response.setSuccess(true);
        return ResponseEntity.ok(response);
    }

    // UNITLAR RO'YXATINI OLISH UCHUN
    public List<Unit> getUnits() {
        return unitRepository.findAll();
    }

    // UNITNI TAHRIRLSH UCHUN
    public Optional<Unit> updateUnit(Integer id, Unit updatedUnit) {
        Optional<Unit> optionalUnit = unitRepository.findById(id);
        if (optionalUnit.isPresent()) {
            Unit unit = optionalUnit.get();
            unit.setName(updatedUnit.getName());
            unit.setCode(updatedUnit.getCode());
            unitRepository.save(unit);
        }
        return optionalUnit;
    }

    // UNITNI O'CHIRISH UCHUN
    public boolean deleteUnit(Integer id){
        if (unitRepository.existsById(id)){
            unitRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
