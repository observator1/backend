package uz.obs.appobservator.service;

import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import uz.obs.appobservator.dto.request.LogConfirmation;
import uz.obs.appobservator.dto.request.LogEnterRequest;
import uz.obs.appobservator.dto.response.ApiResponse;
import uz.obs.appobservator.dto.response.LogRespons3;
import uz.obs.appobservator.dto.response.LogResponse;
import uz.obs.appobservator.dto.response.LogResponse2;
import uz.obs.appobservator.entity.Log;
import uz.obs.appobservator.repository.LogRepository;
import uz.obs.appobservator.security.RoleName;
import uz.obs.appobservator.entity.User;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LogService {
    private final LogRepository logRepository;
    private final SimpMessagingTemplate brokerMessagingTemplate;

    public ResponseEntity<?> enterLog(LogEnterRequest request, HttpServletRequest servletRequest) {

        // LOGNI DB GA SAQLASH
        if (request.getText() == null) {
            return ResponseEntity.status(409).body(
                    new ApiResponse("Text kelmadi", false)
            );
        }
        if (request.getText().isEmpty()) {
            return ResponseEntity.status(409).body(
                    new ApiResponse("Text kelmadi", false)
            );
        }
        String macAddress = request.getField();
        macAddress = macAddress.toUpperCase().replace("$", ":");

        Log log = new Log();
        log.setCreatedAt(log.getCreatedAt());
        log.setSpecialId(request.getText());
        log.setMacAddress(macAddress);
        log.setType(request.getValue());
        log.setIsCommitted(false);
        log.setCode(request.getCode());
        Log savedLog = logRepository.save(log);  // ---> LOG DB GA SAQLANDI


        // LOGNI WEB SOCKET ORQALI JO'NATISH
        LogRespons3 saveLog3 = new LogRespons3();
        saveLog3.setId(log.getId());
        saveLog3.setCreatedAt(log.getCreatedAt());
        saveLog3.setMacAddress(log.getMacAddress());
        saveLog3.setSpecialId(log.getSpecialId());
        saveLog3.setType(log.getType());
        saveLog3.setRegion(logRepository.getAllRegion1());

        System.out.println(saveLog3);

//        brokerMessagingTemplate.convertAndSend("/topic/test", new WebSocketUserDTO(savedLog.getId(), WebSocketType.INDESIT, WsEvent.REFRESH));
        Map<String, Object> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        brokerMessagingTemplate.convertAndSend("/topic/test", saveLog3, header); // ---> LOG WEB SOKKET ORQALLI JO'NATILDI

//        brokerMessagingTemplate.convertAndSend(new WebSocketUserDTO(-1l, WebSocketType.INDESIT, WsEvent.REFRESH));


        ApiResponse response = new ApiResponse();
        response.setMessage("Log saqlandi");
        response.setObj(savedLog);
        response.setSuccess(true);

//        ApiResponse response = ApiResponse.builder()
//                .obj(savedLog)
//                .success(true)
//                .message("Log saqlandi")
//                .build();
        return ResponseEntity.ok(response);
    }

    // ID BO'YICHA LOLGNI OLISH
    public HttpEntity<?> getLog(Integer id, User user) {
        if (user.getRole().equals(RoleName.ROLE_SUPER_ADMIN)) {
            Optional<Log> optional = logRepository.findById(id);
            if (optional.isEmpty()) {
                return ResponseEntity.status(404).body(new ApiResponse("Log topilmadi", false));
            }
            Log log = optional.get();

            LogResponse logRes = new LogResponse();
            logRes.setId(log.getId());
            logRes.setSpecialId(log.getSpecialId());
            logRes.setCreatedAt(log.getCreatedAt());
            logRes.setMacAddress(log.getMacAddress());
            logRes.setType(log.getType());
            return ResponseEntity.ok(new ApiResponse("Log ...", true, logRes));
        } else {
            return ResponseEntity.status(409).body("Sizda ushbu yo'lga murojaat taqiqlangan");
        }
    }

    // BARCHA LOGLAR LISTINI OLISH
    public ApiResponse getLogs(User user) {
        if (user.getRole().equals(RoleName.ROLE_SUPER_ADMIN))
            return new ApiResponse("Barcha loglarni olish", true, logRepository.findAll());
        return new ApiResponse("Ruxsat yuq", true);
    }

    // BARCHA LOGLAR LISTINI OLISH PAGELAB OLISH
    public Page<Log> getAllLogs(int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("createdAt").descending());
        return logRepository.findAll(pageable);
    }


    /*
        public List<Log> getLogs(int page,int size){
            Pageable pageable = PageRequest.of(page, size);
            Page<Log> logsPage=logRepository.findAll(pageable);
            return logsPage.getContent();
        }
        */

    public ApiResponse getRegionName() {
        return new ApiResponse("Oxirgi log regioni", true, logRepository.getAllRegion1());
    }

    // oxirgi log regionini olish
    public ApiResponse getRegionCode() {
        return new ApiResponse("Oxirgi log regioni", true, logRepository.getAllRegion());
    }

    // Okruglar sonini olish diagramma uchun
    public ApiResponse getCounts() {
        return new ApiResponse("Barcha qismalar bo'yicha soni", true, logRepository.getAllCount());
    }

    // OXIRGI 5 TA LOGNI OLISH
    public ApiResponse getlogbesh() {
        List<LogResponse2> response = logRepository.findAllbesh();
        return new ApiResponse("oxirgi beshtasi", true, response);
    }
    public ApiResponse getlogbeshArchive() {
        List<LogResponse2> response = logRepository.findAllbeshArchive();
        return new ApiResponse("Arxivlanmagan oxirgi beshtasi", true, response);
    }


    public ApiResponse getConfirmation(LogConfirmation logConfirmation) {
        if (logConfirmation.getId() <= logRepository.count())
            return new ApiResponse("log Tasdiqlandi", true, logConfirmation.getId());
        return new ApiResponse("Bunday log yo'q", false);
    }


    public ResponseEntity<?> commit(Integer id) {

        Optional<Log> optional = logRepository.findById(id);
        if (optional.isEmpty()) {
            return ResponseEntity.status(404).body(
                    ApiResponse.builder()
                            .message("Log not found")
                            .success(false)
                            .build()
            );
        }

        Log log = optional.get();
        log.setIsCommitted(true);
        Log comittedLog = logRepository.save(log);

        return ResponseEntity.ok(
                ApiResponse.builder()
                        .message("Log comitted")
                        .success(true)
                        .obj(comittedLog)
        );

    }

    public ApiResponse archive(Integer id){
           Optional<Log> optional = logRepository.findById(id);
           if (optional.isPresent()){
               Log log=optional.get();
               log.setArchive(true);
               Log save = logRepository.save(log);
               return ApiResponse.builder()
                       .message("log arxivlandi ")
                       .success(true)
                       .obj(save)
                       .build();
           }
           return ApiResponse.builder()
                   .message("bunday Id lik log yo'q")
                   .success(false)
                   .build();

       }

    public ApiResponse getCounts2() {
        return new ApiResponse("Barcha qismalar bo'yicha soni", true, logRepository.getAllCount2());
    }


}

