package uz.obs.appobservator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import uz.obs.appobservator.repository.UserRepo;
import uz.obs.appobservator.entity.User;


@Service
public class AuthService implements UserDetailsService {

    private final UserRepo userRepo;

    public AuthService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        return userRepo.findByLogin(login);
    }


    public User getById(Integer userId){
        return userRepo.findById(userId).orElseThrow(() -> new IllegalStateException("User with this id does not exist"));
    }
}
