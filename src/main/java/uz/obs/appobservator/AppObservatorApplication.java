package uz.obs.appobservator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.socket.config.annotation.EnableWebSocket;

@SpringBootApplication
@EnableWebMvc
@EnableWebSocket
public class AppObservatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppObservatorApplication.class, args);
    }

}
