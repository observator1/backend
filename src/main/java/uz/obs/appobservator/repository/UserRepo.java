package uz.obs.appobservator.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.obs.appobservator.entity.User;

@Repository
public interface UserRepo extends JpaRepository<User,Integer> {

    User findByLogin(String login);

}
