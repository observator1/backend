package uz.obs.appobservator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.obs.appobservator.security.RoleName;
import uz.obs.appobservator.entity.User;


import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByLogin(String username);

    User findByRole(RoleName admin);


}
