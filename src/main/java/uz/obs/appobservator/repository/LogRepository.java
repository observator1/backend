package uz.obs.appobservator.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.obs.appobservator.dto.response.LogCountDto;
import uz.obs.appobservator.dto.response.LogResponse2;
import uz.obs.appobservator.entity.Log;

import java.util.List;


@Repository
public interface LogRepository extends JpaRepository<Log, Integer> {

    // Ma'lumotlarni olish uchun so'rov
    @Query(value = "SELECT l.specialId FROM Log l limit 100",nativeQuery = true)
    List<String> getAllSpecialId();


//    @Query(value = "select u.id as id,count(*) as count,u.name as name from _log l\n" +
//            "join _units u on SUBSTRING(l.special_id, 4, 1) = u.code\n" +
//            "group by u.id", nativeQuery = true)
//    List<LogCountDto> getAllCount();

    // unitdaga loglaarni soni bo'yicha olish
    @Query(value = "select u.id as id, (" +
            " select count(*) from _log l where SUBSTRING(l.special_id, 4, 1) = u.code " +
            ") as count, u.name as name " +
            "from _units u ", nativeQuery = true)
    List<LogCountDto> getAllCount();

    @Query(value = "SELECT r.region_name as name " +
            "FROM _log l " +
            "JOIN _region r ON SUBSTRING(l.special_id, 2, 2) = r.region_code " +
            "WHERE SUBSTRING(l.special_id, 2, 2) = r.region_code " +
            "ORDER BY l.id DESC " +
            "LIMIT 1", nativeQuery = true)
    String getAllRegion1();

    // REGION CODE NI OLISH
    @Query(value = "SELECT r.region_code as code " +
            "FROM _log l " +
            "JOIN _region r ON SUBSTRING(l.special_id, 2, 2) = r.region_code " +
            "WHERE SUBSTRING(l.special_id, 2, 2) = r.region_code " +
            "ORDER BY l.id DESC " +
            "LIMIT 1", nativeQuery = true)
    String getAllRegion();
    @Query(value = "select l.id as id, l.created_at as createdAt, l.special_id as specialId,\n" +
            "l.mac_address as macAddress, l.type as type from _log l order by id desc LIMIT  5",nativeQuery = true)
    List<LogResponse2> findAllbesh();

    @Query(value = "select l.id as id, l.created_at as createdAt, l.special_id as specialId,\n" +
            "l.mac_address as macAddress, l.type as type from _log l where l.archive=false order by id desc LIMIT  5",nativeQuery = true)
    List<LogResponse2> findAllbeshArchive();


    @Query(value = "select new uz.obs.appobservator.dto.response.LogResponse2(l.id, Format(l.createdAt, 'yyyy-MM-dd HH:mm:ss'), l.specialId, l.macAddress, l.type) from Log l order by l.createdAt desc LIMIT  5",nativeQuery = true)
    List<LogResponse2> findAllbesh2();



    Page<Log> findAll(Pageable pageable);


    // Arxivlanmagan loglarni monitoringni olish
    @Query(value = "select u.id as id, (" +
            " select count(*) from _log l where SUBSTRING(l.special_id, 4, 1) = u.code and l.archive=false" +
            ") as count, u.name as name " +
            "from _units u ", nativeQuery = true)
    List<LogCountDto> getAllCount2();
}
