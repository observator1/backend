package uz.obs.appobservator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.obs.appobservator.entity.Region;

public interface RegionRepository extends JpaRepository<Region, Integer> {

}
