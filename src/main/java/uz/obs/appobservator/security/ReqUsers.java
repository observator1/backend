package uz.obs.appobservator.security;


import lombok.Data;

@Data
public class ReqUsers {
    private Integer id;
    private String firstname;
    private String login;
    private String password;
    private String role;
    private Integer sectionId;


}
