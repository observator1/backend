package uz.obs.appobservator.security;

public enum RoleName {
    ROLE_SUPER_ADMIN("superadmin",0),
    ROLE_ADMIN("admin",1),
    ROLE_USER("user",2);

    private String roleParam;
    private Integer level;
    RoleName(String roleParam) {
        this.roleParam = roleParam;
    }
    RoleName(String roleParam, Integer level) {
        this.level = level;
        this.roleParam = roleParam;
    }
    public String getRoleString()
    {
        return this.roleParam;
    }

    public Integer getLevel() {
        return level;
    }
}
