package uz.obs.appobservator.security;

import lombok.Data;

@Data
public class ReqSignIn {
    private String username;
    private String password;
}
