package uz.obs.appobservator.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "_log")
public class Log {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    // private long time = System.currentTimeMillis();

    private Long createdAt = System.currentTimeMillis();

    @Column(nullable = false)
    private String specialId;

    private String macAddress;

    private String type;

    @Column(nullable = false)
    private Boolean isCommitted = false;

    private Boolean archive=false;
    private String code;



//    public String getCreatedAtString() {
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        return dateFormat.format(createdAt);
//    }



}
