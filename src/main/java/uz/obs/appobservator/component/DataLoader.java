package uz.obs.appobservator.component;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.obs.appobservator.entity.Region;
import uz.obs.appobservator.entity.Unit;
import uz.obs.appobservator.repository.RegionRepository;
import uz.obs.appobservator.repository.UnitRepository;
import uz.obs.appobservator.repository.UserRepository;
import uz.obs.appobservator.security.RoleName;
import uz.obs.appobservator.entity.User;

@Component
@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RegionRepository regionRepository;
    private final UnitRepository unitRepository;

    @Value("${spring.sql.init.mode}")
    private String initMode;

    @Override
    public void run(String... args) throws Exception {

        if (userRepository.findAll().isEmpty()) {


            User user = new User();
            user.setLogin("superadmin");
            user.setName("superadmin");
            user.setPassword(passwordEncoder.encode("03040506qo"));
            user.setRole(RoleName.ROLE_SUPER_ADMIN);
            userRepository.save(user);


            User user1 = new User();
            user1.setLogin("admin");
            user1.setName("Admin");
            user1.setPassword(passwordEncoder.encode("03040506"));
            user1.setRole(RoleName.ROLE_ADMIN);
            userRepository.save(user1);

            User user2 = new User();
            user2.setLogin("admin2");
            user2.setName("Admin2");
            user2.setPassword(passwordEncoder.encode("030405060708"));
            user2.setRole(RoleName.ROLE_ADMIN);
            userRepository.save(user2);

            Region region = new Region();
            region.setRegionCode("68");
            region.setRegionName("Andijon");
            regionRepository.save(region);

            Region region1 = new Region();
            region1.setRegionCode("47");
            region1.setRegionName("Farg'ona");
            regionRepository.save(region1);

            Region region2 = new Region();
            region2.setRegionCode("16");
            region2.setRegionName("Namangan");
            regionRepository.save(region2);

            Region region3 = new Region();
            region3.setRegionCode("23");
            region3.setRegionName("Toshkent");
            regionRepository.save(region3);

            Region region4 = new Region();
            region4.setRegionCode("84");
            region4.setRegionName("Sirdaryo");
            regionRepository.save(region4);

            Region region5 = new Region();
            region5.setRegionCode("21");
            region5.setRegionName("Surxondaryo");
            regionRepository.save(region5);

            Region region6 = new Region();
            region6.setRegionCode("92");
            region6.setRegionName("Qashqadaryo");
            regionRepository.save(region6);

            Region region7 = new Region();
            region7.setRegionCode("49");
            region7.setRegionName("Jizzax");
            regionRepository.save(region7);

            Region region8 = new Region();
            region8.setRegionCode("18");
            region8.setRegionName("Samarqand");
            regionRepository.save(region8);

            Region region9 = new Region();
            region9.setRegionCode("96");
            region9.setRegionName("Buxoro");
            regionRepository.save(region9);

            Region region10 = new Region();
            region10.setRegionCode("75");
            region10.setRegionName("Navoiy");
            regionRepository.save(region10);

            Region region11 = new Region();
            region11.setRegionCode("32");
            region11.setRegionName("Xorazm");
            regionRepository.save(region11);

            Region region12 = new Region();
            region12.setRegionCode("20");
            region12.setRegionName("QR");
            regionRepository.save(region12);

            Region region13 = new Region();
            region13.setRegionCode("62");
            region13.setRegionName("Toshkent shahar");
            regionRepository.save(region13);

            Unit unit=new Unit();
            unit.setName("M va UBAB");
            unit.setCode("Z");
            unitRepository.save(unit);

            Unit unit1=new Unit();
            unit1.setName("THO");
            unit1.setCode("K");
            unitRepository.save(unit1);

            Unit unit2=new Unit();
            unit2.setName("MHO");
            unit2.setCode("C");
            unitRepository.save(unit2);

            Unit unit3=new Unit();
            unit3.setName("JG'MHO");
            unit3.setCode("L");
            unitRepository.save(unit3);

            Unit unit4=new Unit();
            unit4.setName("SHG'HO");
            unit4.setCode("T");
            unitRepository.save(unit4);

            Unit unit5=new Unit();
            unit5.setName("ShHO");
            unit5.setCode("P");
            unitRepository.save(unit5);

            Unit unit6=new Unit();
            unit6.setName("HHM va HHKQ");
            unit6.setCode("F");
            unitRepository.save(unit6);

            Unit unit7=new Unit();
            unit7.setName("TOQQQ");
            unit7.setCode("Q");
            unitRepository.save(unit7);

        }

    }
}

