package uz.obs.appobservator.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogConfirmation {
    private Integer id;
   // private Long time;
}
