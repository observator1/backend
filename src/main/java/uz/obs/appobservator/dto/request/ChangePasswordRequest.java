package uz.obs.appobservator.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Service
@Data
public class ChangePasswordRequest {
    private String oldPassword;
    private String newPassword;
    private String confirmationPassword;
}
