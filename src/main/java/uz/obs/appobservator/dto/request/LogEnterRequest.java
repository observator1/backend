package uz.obs.appobservator.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogEnterRequest {
    private String text;
    private String field;
    private String value;
    private String code;

}
