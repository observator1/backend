package uz.obs.appobservator.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WebSocketUserDTO {

    private Integer id;
    private WebSocketType type;
    private WsEvent evently;

}
