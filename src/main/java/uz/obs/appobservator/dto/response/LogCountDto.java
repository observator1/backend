package uz.obs.appobservator.dto.response;

public interface LogCountDto {
    Integer getId();
    Integer getCount();

    String getName();


}
