package uz.obs.appobservator.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRes {

    private  Integer id;
    private String name;
    private String username;
    private String password;

    private Integer sectionId;


}
