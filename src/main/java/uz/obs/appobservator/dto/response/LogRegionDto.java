package uz.obs.appobservator.dto.response;

public interface LogRegionDto {
    Integer getId();

    String getSpecialId();

    String getRegion();

    String getCreatedAt();
}
