package uz.obs.appobservator.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ApiResponse {

    private String message;
    private Boolean success;
    @JsonProperty("object")
    private Object obj;


    public ApiResponse(String message, Boolean success) {
        this.message = message;
        this.success = success;
    }

}
