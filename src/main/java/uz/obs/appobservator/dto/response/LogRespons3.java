package uz.obs.appobservator.dto.response;

import lombok.*;

import java.sql.Timestamp;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class LogRespons3 {
    private Integer id;
    private String specialId;
    private Long createdAt;
    private String macAddress;
    private String type;
    private String region;


}
