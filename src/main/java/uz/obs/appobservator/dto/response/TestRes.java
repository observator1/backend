package uz.obs.appobservator.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TestRes {

    private Integer id;
    private Timestamp createdAt;
    private String specialId;
    private String macAddress;
    private String type;

}
