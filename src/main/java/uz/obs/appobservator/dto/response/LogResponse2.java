package uz.obs.appobservator.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;


public interface LogResponse2 {
    Integer getId();
    String getCreatedAt();
    String getSpecialId();
    String getMacAddress();
    String getType();
}
