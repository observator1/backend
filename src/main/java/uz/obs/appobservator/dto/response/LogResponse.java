package uz.obs.appobservator.dto.response;

import lombok.*;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class LogResponse {

    private Integer id;
    private String specialId;
    private Long createdAt;
    private String macAddress;
    private String type;
    // private String code;

//    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//    // Timestampni stringga aylantirish
//    String createdAtString = dateFormat.format(new Date(createdAt.getTime()));

//    public String getCreatedAtString() {
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        return dateFormat.format(createdAt);
//    }
    public LogResponse(Integer id, Long createdAt, String specialId, String macAddress, String type) {
        this.id = id;
        this.createdAt = createdAt;
        this.specialId = specialId;
        this.macAddress = macAddress;
        this.type = type;
    }

}