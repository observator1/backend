package uz.obs.appobservator.controller;


import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import uz.obs.appobservator.dto.request.ChangePasswordRequest;
import uz.obs.appobservator.dto.response.UserRes;
import uz.obs.appobservator.entity.User;
import uz.obs.appobservator.security.*;
import uz.obs.appobservator.service.UserService;


// @Api(tags = "Avtorizatsiya",description = "Avtorizatsiya API lar to'plami")
@RestController
@RequestMapping("/api/observatory/auth")
@RequiredArgsConstructor
public class AuthController {


    private final AuthenticationManager authenticationManager;


    private final JwtTokenProvider jwtTokenProvider;

    private final UserService userService;


    @PostMapping("/login")
    public HttpEntity<?> login(@RequestBody ReqSignIn reqSignIn){
        System.out.println("salom");
        Authentication authenticate = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(reqSignIn.getUsername(), reqSignIn.getPassword())
        );
        SecurityContextHolder.getContext().setAuthentication(authenticate);
        String token = jwtTokenProvider.generateToken(authenticate);
        return ResponseEntity.ok(new ApiResponse("Token muvaffaqiyatli olindi",true,token));
    }

    @PostMapping("/register")
    public HttpEntity<?> register(@RequestBody UserRes reqUsers, @CurrentUser User user){
        if (user.getRole().equals(RoleName.ROLE_SUPER_ADMIN)) {
            ApiResponse apiResponse = userService.saveUser(user, reqUsers);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(apiResponse);
        }else return ResponseEntity.status(409).body(new ApiResponse("sizga ushbu yo'lga ruhsat yo'q",false));
    }
    @GetMapping("/me")
    public HttpEntity<?> userMe( @CurrentUser User user){
        return ResponseEntity.status(HttpStatus.OK).body(new ApiResponse("user me",true,user));
    }

    @DeleteMapping("/delete/{id}")
    public HttpEntity<?> deleteUser(@PathVariable Integer id, @CurrentUser User user){
        return ResponseEntity.ok(userService.deleteUser(user,id));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping("/changepassword/{login}")
    public HttpEntity<?> changePassword(@PathVariable String login, @RequestBody ChangePasswordRequest request) {
        if (request.getNewPassword() == null || request.getConfirmationPassword() == null) {
            return ResponseEntity.ok(new ApiResponse("yangi parol null bo'lishi mumkinmas",false));
        }
        if (!request.getNewPassword().equals(request.getConfirmationPassword())) {
            return ResponseEntity.ok(new ApiResponse("yangi parol va tasdiqlash paroli birxilmas",false));
        }
        ApiResponse apiResponse = userService.changePassword(login, request.getOldPassword(), request.getNewPassword());
        return ResponseEntity.ok(apiResponse);
    }
}
