package uz.obs.appobservator.controller;

import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.obs.appobservator.dto.request.UnitDto;
import uz.obs.appobservator.dto.response.ApiResponse;
import uz.obs.appobservator.entity.Unit;
import uz.obs.appobservator.entity.User;
import uz.obs.appobservator.service.UnitService;


import java.util.List;
import java.util.Optional;

// @Api(tags = "Unit",description = "qism API lar to'plami")
@RestController
@RequiredArgsConstructor
//@PreAuthorize(value = "hasAuthority('SUPER_ADMIN')")
@RequestMapping("/api/observatory")
public class UnitController {
    private final UnitService unitService;


    @PostMapping("/addUnit")
    public HttpEntity<?> addUnit(@RequestBody UnitDto unitsDto, User user){
        return unitService.addUnit(unitsDto);
    }

    @GetMapping("/units")
    public HttpEntity<?> viewLogs(){
        List<Unit> unitList=unitService.getUnits();
        return ResponseEntity.ok(unitList);
    }

    @PutMapping("/updeteUnit/{id}")
    public ResponseEntity<ApiResponse> updateRegion(@PathVariable Integer id, @RequestBody Unit updatedUnit) {
        Optional<Unit> unit = unitService.updateUnit(id, updatedUnit);
        if (unit.isPresent()) {
            return ResponseEntity.ok(new ApiResponse("Unit tahrirlandi",true, unit.get()));
        } else {
            return ResponseEntity.ok(new ApiResponse("bunday unit topilmadi ",false, null));
        }
    }

    @DeleteMapping("/deleteUnit/{id}")
    public HttpEntity<?>deleteUnit(@PathVariable Integer id) {
        if (unitService.deleteUnit(id)) {
            return ResponseEntity.ok(new ApiResponse("Unit o'chirildi", true));
        } else {
            return ResponseEntity.ok(new ApiResponse("bunday Unit yo'q", false));
        }
    }


}
