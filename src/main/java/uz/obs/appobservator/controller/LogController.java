package uz.obs.appobservator.controller;

import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.obs.appobservator.dto.request.LogConfirmation;
import uz.obs.appobservator.dto.request.LogEnterRequest;
import uz.obs.appobservator.dto.response.ApiResponse;
import uz.obs.appobservator.entity.Log;
import uz.obs.appobservator.security.CurrentUser;
import uz.obs.appobservator.entity.User;
import uz.obs.appobservator.security.RoleName;
import uz.obs.appobservator.service.LogService;

// @Api(tags = "Log",description = "loglar API lar to'plami")
@RestController
@RequestMapping("api/observatory/log")
@RequiredArgsConstructor
public class LogController {

    private final LogService logService;
    // private final SimpMessagingTemplate simpMessagingTemplate;
    // LOG QO'SHISH
    @PostMapping("/enter")
    public HttpEntity<?> enterLog(@RequestBody LogEnterRequest request, HttpServletRequest servletRequest) {
//        simpMessagingTemplate.convertAndSend("/topic/test", logService.enterLog(request, servletRequest));
        return  logService.enterLog(request, servletRequest);
    }

    @GetMapping("/get/{id}")
    public HttpEntity<?> getLog(@PathVariable Integer id,@CurrentUser User user) {
        return logService.getLog(id, user);
    }



    // BARCHA LOGLAR LISTINI OLISH
   // @PreAuthorize(value = "hasAuthority('SUPER_ADMIN')")
    @GetMapping("/logs1")
    public HttpEntity<?> getLogs1(@CurrentUser User user) {
        ApiResponse logs = logService.getLogs(user);
        return ResponseEntity.ok(logs);
    }



    // BARCHA LOGLAR LISTINI OLISH PAGLAB OLISH
    @GetMapping("/logs")
    public ResponseEntity<?> getLogs(@RequestParam(defaultValue = "0") int page,@RequestParam(defaultValue = "10") int size ){
       Page<Log> logs=logService.getAllLogs(page,size);
       return ResponseEntity.ok(logs);
    }


    @GetMapping("/noted/{id}")
    public ResponseEntity<?> commitLog(@CurrentUser User user,@PathVariable Integer id, @RequestParam(defaultValue = "0") int page,@RequestParam(defaultValue = "10") int size){
        if (user.getRole().equals(RoleName.ROLE_SUPER_ADMIN)) {
       logService.commit(id);
        Page<Log> logs=logService.getAllLogs(page,size);
        return ResponseEntity.ok(logs);
    }
        return ResponseEntity.ok(ApiResponse.builder()
                .message("sizga ushbu yo'lga ruhsat yo'q")
                .success(false)
                .build());
    }

    // OXIRGI 5 TA LOGNI OLISH
    @GetMapping("/logbesh1")
    public HttpEntity<?> getLogbesh() {
        ApiResponse logbesh= logService.getlogbesh();
        return ResponseEntity.ok(logbesh);
    }

    // OXIRGI 5 TA LOGNI OLISH
    @GetMapping("/logbesh")
    public HttpEntity<?> getLogbesharchive() {
        ApiResponse logbeshArchive= logService.getlogbeshArchive();
        return ResponseEntity.ok(logbeshArchive);
    }

    // Monitoring qismi
    @GetMapping("/count2")
    public HttpEntity<?> viewCounts(){
        ApiResponse countList=logService.getCounts();
        return ResponseEntity.ok(countList);
    }


   // @PreAuthorize(value = "hasAnyAuthority('SUPER_ADMIN', 'ADMIN')")
    @GetMapping("/logRegionCode")
    public HttpEntity<?> logRegionCode(){
        ApiResponse logRegionCode=logService.getRegionCode();
        return ResponseEntity.ok(logRegionCode);
    }

    @GetMapping("/LogConfirmation")
    public ApiResponse logConfirmation(@RequestBody LogConfirmation logConfirmation){
        return logService.getConfirmation(logConfirmation);
    }

    @GetMapping("/archivelog/{id}")
    public HttpEntity<?>archiveLog(@CurrentUser User user,@PathVariable Integer id, @RequestParam(defaultValue = "0") int page,@RequestParam(defaultValue = "10") int size){
        if (user.getRole().equals(RoleName.ROLE_SUPER_ADMIN)) {
            logService.archive(id);
            Page<Log> logs = logService.getAllLogs(page, size);
            return ResponseEntity.ok(logs);
        }
        return ResponseEntity.ok(ApiResponse.builder()
                .message("sizga ushbu yo'lga ruhsat yo'q")
                .success(false)
                .build());
    }

    // arxivlanmagan loglarni okrug bo'yicha sonini olish
    @GetMapping("/count")
    public HttpEntity<?> viewCounts2(){
        ApiResponse countList2=logService.getCounts2();
        return ResponseEntity.ok(countList2);
    }

}
