package uz.obs.appobservator.controller;

import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.obs.appobservator.dto.request.RegionDto;
import uz.obs.appobservator.dto.response.ApiResponse;
import uz.obs.appobservator.entity.Region;
import uz.obs.appobservator.entity.User;
import uz.obs.appobservator.service.RegionService;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/observatory")
//@PreAuthorize(value = "hasAuthority('SUPER_ADMIN')")
public class RegionController {
    private final RegionService regionService;

    @PostMapping("/addRegion")
    public HttpEntity<?> addRegion(@RequestBody RegionDto regionDto, User user){
        ApiResponse apiResponse=regionService.addRegion(regionDto);
        return ResponseEntity.ok(apiResponse);
    }

    @GetMapping("/getRegions")
    public HttpEntity<?>getRegions(){
        ApiResponse apiResponse=regionService.getRegion();
        return ResponseEntity.ok(apiResponse);
    }

    @PutMapping("/updeteRegion/{id}")
    public ResponseEntity<ApiResponse> updateRegion(@PathVariable Integer id, @RequestBody RegionDto updatedRegionDto) {
        Optional<Region> region = regionService.updateRegion(id, updatedRegionDto);
        return region.map(value -> ResponseEntity.ok(new ApiResponse("Region tahrirlandi", true, value))).orElseGet(() -> ResponseEntity.ok(new ApiResponse("bunday region topilmadi ", false, null)));
//        if (region.isPresent()) {
//            return ResponseEntity.ok(new ApiResponse("Region tahrirlandi",true, region.get()));
//        } else {
//            return ResponseEntity.ok(new ApiResponse("bunday region topilmadi ",false, null));
//        }
    }

    @DeleteMapping("/deleteRegion/{id}")
    public HttpEntity<?>deleteRegion(@PathVariable Integer id){
        if (regionService.deleteRegion(id)){
            return ResponseEntity.ok(new ApiResponse("Region o'chirildi",true));
        }
        else {
            return ResponseEntity.ok(new ApiResponse("Bunday Region topilmadi",false));
        }
    }

}
